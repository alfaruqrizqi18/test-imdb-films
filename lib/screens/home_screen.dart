// ignore_for_file: prefer_const_constructors

import 'dart:convert';

import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("IMDB Popular Films"),
        scrolledUnderElevation: 1,
        shadowColor: Colors.grey,
      ),
      body: FutureBuilder(
        future: DefaultAssetBundle.of(context).loadString(
          'assets/json/movie.json',
        ),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          var newData = json.decode(snapshot.data.toString());
          if (newData == null) {
            return SizedBox();
          }
          return ListView.builder(
            itemCount: newData['items'].length,
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 10,
            ),
            itemBuilder: (context, index) {
              var item = newData['items'][index];
              return Card(
                margin: const EdgeInsets.only(bottom: 15),
                child: Container(
                  padding: const EdgeInsets.all(8),
                  child: Row(
                    children: [
                      Container(
                        child: Image.network(
                          item['image'],
                          height: 60,
                          width: 60,
                          fit: BoxFit.cover,
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                item['fullTitle'],
                                textAlign: TextAlign.left,
                              ),
                              Text(
                                item['year'],
                                textAlign: TextAlign.left,
                              ),
                              Text(
                                item['imDbRating'],
                                textAlign: TextAlign.left,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        child: Builder(
                          builder: (context) {
                            if (item['rankUpDown'].toString().contains("+")) {
                              return Icon(
                                Icons.arrow_upward,
                                color: Colors.green,
                              );
                            }
                            if (item['rankUpDown'].toString().contains("-")) {
                              return Icon(
                                Icons.arrow_downward,
                                color: Colors.red,
                              );
                            }
                            return Icon(
                              Icons.minimize,
                              color: Colors.grey,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
